import * as angular from 'angular';
import { downgradeComponent, downgradeInjectable } from '@angular/upgrade/static';
import { ColorHomeComponent } from './color-home.component';

import { ColorsService } from './colors.service';
import { ToolHeaderComponent as ToolHeaderComponentOld } from './tool-header.component';

import { ColorsService as ColorsService2 }  from '../app/services/colors.service';
import { ToolHeaderComponent } from '../app/components/tool-header/tool-header.component';
import { ColorListComponent } from '../app/components/color-list/color-list.component';



angular.module('colorToolModule', [])
  .service('colorsSvc', ColorsService)
  .component('colorHome', ColorHomeComponent)
  .component('toolHeaderOld', ToolHeaderComponentOld)
  .directive('toolHeader', downgradeComponent({ component: ToolHeaderComponent }))
  .directive('colorList', downgradeComponent({ component: ColorListComponent }))
  .service('colorsSvc2', downgradeInjectable(ColorsService2));