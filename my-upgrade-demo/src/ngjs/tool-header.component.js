import template from './tool-header.component.html';

export const ToolHeaderComponent = {
  template,
  bindings: {
    headerText: '<'
  },
  controller: class {
    
  }
};