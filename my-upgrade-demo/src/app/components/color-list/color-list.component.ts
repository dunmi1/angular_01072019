import { Injectable, Component, OnInit, Input } from '@angular/core';

@Injectable({
  providedIn: 'root',
  useFactory: (i: { get: (svcName: string) => object }) => i.get('colorsSvc'),
  deps: ['$injector'],
})
abstract class ColorsService {
  abstract all(): string[];
}

@Component({
  selector: 'color-list',
  templateUrl: './color-list.component.html',
  styleUrls: ['./color-list.component.css']
})
export class ColorListComponent implements OnInit {

  colors: string[] = [];

  constructor(private colorsSvc: ColorsService) {
    this.colors = colorsSvc.all();
  }

  ngOnInit() {
  }

}
