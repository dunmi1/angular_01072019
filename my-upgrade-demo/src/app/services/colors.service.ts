import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorsService {

  _colors = ['red','blue','green'];

  constructor() { }

  all() {
    console.log('angular color svc');
    return this._colors.concat();
  }
}
