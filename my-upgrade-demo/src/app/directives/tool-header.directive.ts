import { Directive, ElementRef, Injector, Input } from '@angular/core';
import { UpgradeComponent } from '@angular/upgrade/static';

@Directive({
  selector: 'tool-header-old'
})
export class ToolHeaderDirective extends UpgradeComponent {

  @Input()
  headerText = 'Tool Header';

  constructor(elementRef: ElementRef, injector: Injector) {
    super('toolHeaderOld', elementRef, injector);
  }

}
