import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SapphireComponent } from './sapphire.component';

describe('SapphireComponent', () => {
  let component: SapphireComponent;
  let fixture: ComponentFixture<SapphireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SapphireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SapphireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
