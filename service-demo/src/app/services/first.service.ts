import { Injectable } from '@angular/core';

let counter = 0;

class AltFirstService {
  doIt() {
    console.log('alt did it');
  }
}

const altFirstService = {
  doIt() {
    console.log('alt value did it');
  }
};

const getMeFirstService = () => {
  return new AltFirstService();
};

export interface ILogSvc {
  doIt: () => void;
}

@Injectable({
  providedIn: 'root',
  // useClass: AltFirstService,
  // useValue: altFirstService,
  // useFactory: getMeFirstService,
})
export class FirstService implements ILogSvc {

  myCounter = counter++;

  constructor() { }

  doIt() {
    console.log('did it', this.myCounter);
  }
}
