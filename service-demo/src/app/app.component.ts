import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { FirstService } from './services/first.service';

// class AltFirstService {
//   doIt() {
//     console.log('alt did it');
//   }
// }

// const altFirstService = {
//   doIt() {
//     console.log('alt value did it');
//   }
// };

// const getMeFirstService = () => {
//   return new AltFirstService();
// };

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // providers: [
    // FirstService,
    // { provide: FirstService, useClass: AltFirstService },
    // { provide: FirstService, useValue: altFirstService },
    // { provide: FirstService, useFactory: getMeFirstService },
  // ]
})
export class AppComponent implements OnInit {
  title = 'service-demo';

  contactForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.contactForm = this.fb.group({
      fullName: [ 'Eric Greene', { validators: [ Validators.required ] } ],
    });
  }

  doOutput() {
    console.log(this.contactForm.value);
  }
}
