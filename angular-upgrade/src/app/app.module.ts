import { BrowserModule } from '@angular/platform-browser';
import { UpgradeModule } from '@angular/upgrade/static';
import { NgModule } from '@angular/core';
import { ToolHeaderComponent } from './components/tool-header/tool-header.component';
import { ColorListComponent } from './components/color-list/color-list.component';
import { ToolHeaderLegacyDirective } from './directives/tool-header-legacy.directive';

@NgModule({
  declarations: [ ToolHeaderComponent, ColorListComponent, ToolHeaderLegacyDirective ],
  imports: [
    BrowserModule, UpgradeModule,
  ],
  providers: [],
  entryComponents: [ ToolHeaderComponent, ColorListComponent   ],
})
export class AppModule {

  constructor(private upgrade: UpgradeModule) {
  }

  ngDoBootstrap() {
    this.upgrade.bootstrap(document.querySelector('app-root'), ['colorToolApp']);
  }


}
