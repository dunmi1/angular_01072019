import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationCancel } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  routerEventsSubscription: Subscription;

  constructor(private router: Router) { }

  ngOnInit() {
    this.routerEventsSubscription = this.router.events.pipe(
      filter(e => e instanceof NavigationCancel)
    ).subscribe(e => {
      // console.log(e);
    });
  }

  ngOnDestroy() {
    this.routerEventsSubscription.unsubscribe();
  }

}
