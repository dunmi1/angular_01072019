import { TestBed } from '@angular/core/testing';

import { NumResolveService } from './num-resolve.service';

describe('NumResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NumResolveService = TestBed.get(NumResolveService);
    expect(service).toBeTruthy();
  });
});
