import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';

import { AuthGuardService } from './services/auth-guard.service';
import { NumResolveService } from './services/num-resolve.service';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent, canActivate: [ AuthGuardService ] },
  { path: 'about/:id', component: AboutComponent, canActivate: [ AuthGuardService ] },
  { path: 'contact', component: ContactComponent, resolve: { num: NumResolveService } },
  { path: '**', redirectTo: '/home' },
];

export const AppRoutingModule = RouterModule.forRoot(routes, { enableTracing: false });
