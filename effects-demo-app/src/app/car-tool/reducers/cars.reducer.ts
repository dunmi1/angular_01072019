import {
  CarActionTypes, CarActionUnion, RefreshDoneCarAction,
} from '../car.actions';

import { Car } from '../models/car';

export const carsReducer = (state: Car[] = [], action: CarActionUnion) => {

  if (action.type === CarActionTypes.REFRESH_DONE) {
    return (action as RefreshDoneCarAction).payload;
  }

  return state;

};
