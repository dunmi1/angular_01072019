import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { memoize } from 'lodash';

import { Car } from '../../models/car';

@Component({
  selector: 'car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.css'],
})
export class CarTableComponent implements OnInit {

  sortField = 'id';
  filterField = '';
  filterValue = '';

  _cars: Car[] = [];

  filterForm: FormGroup;

  @Input()
  editCarId = 0;

  @Output()
  editCar = new EventEmitter<number>();

  @Output()
  deleteCar = new EventEmitter<number>();

  @Output()
  saveCar = new EventEmitter<Car>();

  @Output()
  cancelCar = new EventEmitter<void>();

  sortCars: { (sortField: string, carsFn: any): Car[], cache: { clear: () => void } };

  filterCars: { (filterField: string, filterValue: string, cars: Car[]): Car[], cache: { clear: () => void } };


  _sortCars(sortField: string, carsFn: any) {
    console.log(carsFn);
    return (typeof carsFn === 'function' ? carsFn() : carsFn).concat().sort((a, b) => {
      if (a[sortField] < b[sortField]) {
        return -1;
      } else if (a[sortField] > b[sortField]) {
        return 1;
      } else {
        return 0;
      }
    });
  }



  _filterCars(filterField: string, filterValue: string, cars: Car[]) {

    console.log(cars);

    if (!filterField || !filterValue) {
      return cars;
    }

    return cars.filter( c => String(c[filterField]).startsWith(filterValue) );
  }


  @Input()
  set cars(value: Car[]) {
    if (value !== this._cars) {
      this._cars = value;
      this.sortCars.cache.clear();
      this.filterCars.cache.clear();
    }
  }

  get cars() {
    return this.sortCars(this.sortField, () => this.filterCars(this.filterField, this.filterValue, this._cars));
  }


  constructor(private fb: FormBuilder) {
    this.sortCars = memoize(this._sortCars, sf => sf);
    this.filterCars = memoize(this._filterCars, (filterField: string, filterValue: string) => filterField + filterValue);
  }

  ngOnInit() {
    this.filterForm = this.fb.group({
      filterField: '',
      filterValue: '',
    });
  }

  doFilter() {
    this.filterField = this.filterForm.value.filterField;
    this.filterValue = this.filterForm.value.filterValue;

    this.sortCars.cache.clear();
  }

  doEdit(carId: number) {
    this.editCar.emit(carId);
  }

  doDelete(carId: number) {
    this.deleteCar.emit(carId);
  }

  doSave(car: Car) {
    this.saveCar.emit(car);
  }

  doCancel() {
    this.cancelCar.emit();
  }

}
