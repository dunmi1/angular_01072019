import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap, debounceTime, map } from 'rxjs/operators';

interface Color {
  id?: number;
  name: string;
}

@Component({
  selector: 'typeahead-demo',
  templateUrl: './typeahead-demo.component.html',
  styleUrls: ['./typeahead-demo.component.css']
})
export class TypeaheadDemoComponent implements OnInit {

  colorSearchInput = new FormControl();

  colorSearch$ = new BehaviorSubject<string | Color[]>([]);

  colors$: Observable<Color[]> = this.colorSearch$.pipe(
    debounceTime(200),
    switchMap( (colorSearchText: string) => {
      return this.httpClient.get<Color[]>('http://localhost:4250/colors?name_like=' + colorSearchText);
    }),
    map( (colors: Color[]) => colors),
  );

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }

  doColorSearch() {
    this.colorSearch$.next(this.colorSearchInput.value);
  }

  colorNameTrack(index: number, color: Color) {
    return color.name;
  }




}
