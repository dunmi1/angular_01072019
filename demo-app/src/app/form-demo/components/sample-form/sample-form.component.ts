import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

const myRequired = (c: FormControl) => {

  if (c.value == null || String(c.value).length === 0) {
    // invalid
    return {
      myRequired: true
    };
  }

  // valid
  return null;

};

const phoneOrEmailRequired  = (g: FormGroup) => {

  if (g.value.phone || g.value.email) {
    return null;
  }

  return { phoneOrEmailRequired: true };

};


@Component({
  selector: 'sample-form',
  templateUrl: './sample-form.component.html',
  styleUrls: ['./sample-form.component.css']
})
export class SampleFormComponent implements OnInit {

  firstNameInput: FormControl;

  sampleForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.sampleForm = this.fb.group({
      // firstName: [ '', {
      //   validators: [
      //     // Validators.required,
      //     myRequired,
      //     Validators.minLength(3)
      //   ],
      // }],
      // lastName: [ '' ],
      email: '',
      phone: '',
      // address: this.fb.group({
      //   street: '123 Oak Lane',
      //   city: 'Morrisville',

      // })
    }, { validator: [ phoneOrEmailRequired ] });

    // this.firstNameInput = this.fb.control('', {
    //   validators: [
    //     // Validators.required,
    //     myRequired,
    //     Validators.minLength(3)
    //   ],
    // });
    // this.firstNameInput = new FormControl('', {
    //   validators: [
    //     // Validators.required,
    //     myRequired,
    //     Validators.minLength(3)
    //   ],
    // });


    this.firstNameInput = new FormControl('test');

    this.firstNameInput.setValue('test2');
  }

  get isFirstNameRequiredInvalid() {
    return false;
    // return this.firstNameInput.errors &&
    //   this.firstNameInput.errors.myRequired &&
    //   this.firstNameInput.touched;
  }

  get isFirstNameMinLengthInvalid() {
    return false;
    // return this.firstNameInput.errors &&
    //   this.firstNameInput.errors.minlength &&
    //   this.firstNameInput.touched;
  }

}
