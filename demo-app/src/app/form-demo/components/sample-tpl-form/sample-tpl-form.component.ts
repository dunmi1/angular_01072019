import { Component, OnInit, ViewChild, AfterViewInit, Directive } from '@angular/core';
import { NgModel, NG_VALIDATORS, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

const myRequiredValidator = (c: FormControl) => {

  if (c.value == null || String(c.value).length === 0) {
    // invalid
    return {
      myRequired: true
    };
  }

  // valid
  return null;

};

@Directive({
  selector: '[ngModel][myRequired]',
  providers: [ { provide: NG_VALIDATORS, multi: true, useValue: myRequiredValidator } ],
})
export class MyRequiredValidatorDirective  { }

@Component({
  selector: 'sample-tpl-form',
  templateUrl: './sample-tpl-form.component.html',
  styleUrls: ['./sample-tpl-form.component.css']
})
export class SampleTplFormComponent implements OnInit, AfterViewInit {

  @ViewChild(NgModel)
  firstNameModel: NgModel;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    console.log(this.firstNameModel);
  }

}
