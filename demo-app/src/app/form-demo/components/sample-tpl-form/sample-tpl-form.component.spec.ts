import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleTplFormComponent } from './sample-tpl-form.component';

xdescribe('SampleTplFormComponent', () => {
  let component: SampleTplFormComponent;
  let fixture: ComponentFixture<SampleTplFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleTplFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleTplFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
