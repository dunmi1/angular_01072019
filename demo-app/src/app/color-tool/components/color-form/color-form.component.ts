import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: '.color-form',
  templateUrl: './color-form.component.html',
  styleUrls: ['./color-form.component.css']
})
export class ColorFormComponent implements OnInit {

  @Input()
  buttonText = 'Submit Color';

  @Output()
  submitColor = new EventEmitter<string>();

  colorForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.colorForm = this.fb.group({
      color: '',
    });
  }

  doSubmitColor() {
    this.submitColor.emit(this.colorForm.value.color);

    this.colorForm.reset();
  }
}
